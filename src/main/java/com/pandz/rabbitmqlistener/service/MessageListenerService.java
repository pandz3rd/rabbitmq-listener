package com.pandz.rabbitmqlistener.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pandz.rabbitmqlistener.constant.Constant;
import com.pandz.rabbitmqlistener.model.EmailModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageListenerService {
  private final static Logger log = LoggerFactory.getLogger(MessageListenerService.class);

  private final ObjectMapper mapper;

  @Autowired
  public MessageListenerService(ObjectMapper mapper) {
    this.mapper = mapper;
  }

  @RabbitListener(queues = Constant.HELLO_JANGKRIK)
  public void greeting(String message) {
    log.info("Receiving message....");
    log.info("Message is: " + message);
  }

  @RabbitListener(queues = Constant.EMAIL)
  public void receiveEmail(String message) {
    log.info("Receiving message....");
    try {
      EmailModel emailModel = mapper.readValue(message, EmailModel.class);
      log.info("Converting to model....");
      log.info("Message is: " + emailModel.toString());
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
  }
}
